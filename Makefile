.PHONY: main watch clean

PREFIX := $(HOME)/texmf/tex/latex
INSTALL_MODE := symlink

watch:
	find -name "*.tex" -or -name "*.sty" | entr -rc make build

build: 
	pdflatex main.tex
	pdflatex main.tex

clean:
	latexmk -c
	rm main.bbl -f
	rm main.snm -f
	rm main.run.xml -f
	rm main.run.xml -f

view:
	qpdfview main.pdf

install:
	echo $(PREFIX)
ifeq ($(INSTALL_MODE), symlink)
	ln -srf beamercolortheme420450.sty "$(PREFIX)/"
	ln -srf beamerfonttheme420450.sty "$(PREFIX)/"
	ln -srf beamerinnertheme420450.sty "$(PREFIX)/"
	ln -srf beameroutertheme420450.sty "$(PREFIX)/"
	ln -srf beamertheme420450.sty "$(PREFIX)/"
else ifeq($(INSTALL_MODE), copy)
	cp beamercolortheme420450.sty "$(PREFIX)/"
	cp beamerfonttheme420450.sty "$(PREFIX)/"
	cp beamerinnertheme420450.sty "$(PREFIX)/"
	cp beameroutertheme420450.sty "$(PREFIX)/"
	cp beamertheme420450.sty "$(PREFIX)/"
endif
